---
name: Dustin Ingram
talks:
- 'Inside the Cheeseshop: How Python Packaging Works'
---

I’m Dustin (aka [@di](<https://github.com/di>)), a Developer Advocate at
Google, focused on supporting the Python community on the Google Cloud
Platform. I’m also a member of the [Python Packaging
Authority](https://github.com/orgs/pypa/people), maintainer of the [Python
Package Index](https://pypi.org/), and organizer for the
[PyTexas](https://www.pytexas.org) conference.
