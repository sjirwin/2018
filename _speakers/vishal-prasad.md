---
name: Vishal Prasad
talks:
- From Coroutines to Concurrency
---

I am a Platform Engineer at Peloton. I work on scaling distributed databases, cleaning up our analysis pipeline, and punching race-conditions in the face.