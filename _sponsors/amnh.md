---
name: "AMNH's BridgeUP: STEM"
tier: young coders
site_url: https://www.amnh.org/learn-teach/bridgeup-stem
logo: bridgeup.png
---
AMNH’s BridgeUP: STEM is focused on the intersection of computer science and science. The program
includes: a tuition-free intensive three-year Brown Scholars program for high school girls; the
Helen Fellowship, a one-year post-baccalaureate fellowship for women; computational science
workshops for middle schoolers; and an annual overnight Hackathon for professional developers
addressing computational challenges in scientific discipline.

BridgeUP: STEM is generously supported by a grant from the Helen Gurley Brown Revocable Trust to
bridge the gender and opportunity gaps in computer science and science.
