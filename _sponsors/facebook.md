---
name: Facebook
tier: silver
site_url: https://www.facebook.com/careers
logo: facebook.png
---
Facebook’s mission is to give people the power to build community and bring the world closer
together. People use Facebook to stay connected with friends and family, to discover what’s going on
in the world, and to share and express what matters to them.

Facebook is defined by our unique culture – one that rewards impact. We encourage people to be bold
and solve the problems they care most about. We work in small teams and move fast to develop new
products, constantly iterating. The phrase “this journey is 1% finished,” reminds us that we’ve only
begun to fulfill our mission to bring the world closer together.
