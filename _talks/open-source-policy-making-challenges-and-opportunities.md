---
abstract: "Public policy analysis has typically been conducted under a shroud of secrecy
  by three letter agencies and think tanks in DC using black box models. In this talk,
  I\u2019ll cover the issues with this approach and the benefits and complications
  of moving toward open source policy analysis."
duration: 30
level: All
room: Madison
slot: 2018-10-06 15:10:00-04:00
speakers:
- Anderson Frailey
title: Open Source Policy Making - Challenges and Opportunities
type: talk
video_url: https://youtu.be/KJNMT196Z7I
---

Policy analysis typically follows a simple pattern: legislation is proposed, think tanks and government agencies produce an analysis of the bill, and then journalists and pundits debate its merits using numbers whose source is opaque at best. This approach opens the door for otherwise sound analysis to be dismissed by those how don’t line the findings or for actual flaws to go unnoticed. But lately there has been some movement toward more open policy analysis which has increased both its rigor and potential to be communicated effectively. This talk will layout the problems with a black-box approach to policy analysis, projects and initiatives aimed at cracking said black-box, and some of the complications that come with moving toward open source.