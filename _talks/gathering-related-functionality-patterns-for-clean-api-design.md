---
abstract: This talk will arm you with some tools to design a library that "just works",
  but also has obvious escape hatches to handle corner cases. It covers several patterns
  for cleanly organizing related and overlapping functionality in a way that statisfies
  both humans and static analysis tools.
duration: 25
level: Intermediate
room: Madison
slot: 2018-10-05 14:00:00-04:00
speakers:
- Paul Ganssle
title: 'Gathering Related Functionality: Patterns for Clean API Design'
type: talk
video_url: https://youtu.be/nmoT-E8fi_8
---

What do you do when you have to choose between designing your function for one of two common use cases?

How about when the same logical operations (say, multiplication or concatenation) need to have different implementations depending on the type of the arguments they are applied to?

These kinds of questions can be vexing when trying to design a clean, well-scoped API.

This talk will cover several strategies for grouping related functionality in a way that presents a logically clean interface to both humans and static analysis tools like type checkers and document generators.

This talk covers:

- Alternate constructors with `@classmethod`
- Namespacing functions under a class with `@staticmethod`
- Dispatch by type
- A new convention for namespacing functions: `variants`